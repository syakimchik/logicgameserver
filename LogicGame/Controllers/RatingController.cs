﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LogicGame.Domain.Database;
using LogicGame.Domain.Repository;
using LogicGame.Models;

namespace LogicGame.Controllers
{
    public class RatingController : ApiController
    {
        private PlayerRepository repo = new PlayerRepository();
        private ScoreRepository scoreRepo = new ScoreRepository();

        [HttpPost]
        public HttpResponseMessage Post([FromBody]RatingModel value)
        {
            if (value.Info.Id == -1)
            {
                GameRepository gameRepository = new GameRepository();
                Player player1 = new Player()
                {
                    GameId = gameRepository.GetByName(value.Info.GameName).Id,
                    Name = value.Info.NickName,
                    Score = new Score()
                    {
                        Count = value.Info.Score
                    }
                };
                PlayerRepository repo1 = new PlayerRepository();
                repo1.Create(player1);
                return Request.CreateResponse(HttpStatusCode.OK, player1.Id);
            }
            Player player = repo.GetById(value.Info.Id);
            player.Score.Count = value.Info.Score;
            scoreRepo.Update(player.Score); 
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }

        [HttpGet]
        public RatingModel[] GetRatings(string game)
        {
            List<RatingModel> list = new List<RatingModel>();
            PlayerRepository repo1 = new PlayerRepository();
            IEnumerable<Player> players = repo1.GetAll().Where(x => x.Game.Name.Equals(game));
            foreach (var player in players)
            {
                list.Add(new RatingModel()
                {
                    Info = new InfoModel()
                    {
                        GameName = game,
                        NickName = player.Name,
                        Id = player.Id,
                        Score = player.Score.Count
                    }
                });
            }

            return list.OrderByDescending(x => x.Info.Score).ToArray();
        }
    }
}
