﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LogicGame.Domain.Database;
using LogicGame.Domain.Repository;

namespace LogicGame.Controllers
{
    public class GameController : ApiController
    {
        private GameRepository repo = new GameRepository();

        [HttpGet]
        public IEnumerable<Game> GetAllGames()
        {
            return repo.GetAll();
        } 

        [HttpGet]
        public Game GetById(int id)
        {
            return repo.GetById(id);
        } 
    }
}
