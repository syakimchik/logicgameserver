﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogicGame.Models
{
    public class InfoModel
    {
        public string GameName { get; set; }
        public string NickName { get; set; }
        public int Id { get; set; }
        public int Score { get; set; }
    }

    public class RatingModel
    {
        public InfoModel Info { get; set; }
    }
}