﻿using System;
using System.Net.Http;
using System.Web.Http;
using LogicGame.Controllers;
using LogicGame.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogicGame.Tests
{
    [TestClass]
    public class WebApiTest
    {
        [TestMethod]
        public void PostRatingData()
        {
            RatingController controller = new RatingController();

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            RatingModel rating = new RatingModel()
            {
                Info = new InfoModel()
                {
                    GameName = "LogicGame",
                    Id = -1,
                    NickName = "Nick",
                    Score = 376
                }
            };
            var response = controller.Post(rating);
        }
    }
}
