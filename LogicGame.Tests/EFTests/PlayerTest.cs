﻿using System;
using LogicGame.Domain.Database;
using LogicGame.Domain.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogicGame.Tests
{
    [TestClass]
    public class PlayerTest
    {
        [TestMethod]
        public void CreatePlayer()
        {
            string gameName = "LogicGame";
            int count = 90;
            GameRepository gameRepository = new GameRepository();
            Player player = new Player()
                            {
                                GameId = gameRepository.GetByName(gameName).Id,
                                Name = "Player 1",
                                Score = new Score()
                                        {
                                            Count = count
                                        }
                            };
            PlayerRepository repo = new PlayerRepository();
            repo.Create(player);
        }

        [TestMethod]
        public void UpdatePlayer()
        {

        }

        [TestMethod]
        public void DeletePlayer()
        {

        }

        [TestMethod]
        public void GetAllPlayers()
        {

        }
    }
}
