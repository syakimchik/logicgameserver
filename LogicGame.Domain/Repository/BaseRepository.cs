﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogicGame.Domain.Database;
using LogicGame.Domain.Interfaces;

namespace LogicGame.Domain.Repository
{
    public abstract class BaseRepository<T>: IRepository<T>
    {
        protected DatabaseModelContainer _dbContext;

        protected BaseRepository()
        {
            _dbContext = new DatabaseModelContainer();
        }

        public abstract IEnumerable<T> GetList();

        public abstract T GetById(long id);

        public abstract void Create(T entity);

        public abstract void Update(T entity);

        public abstract void Delete(T entity);
        
        public abstract IEnumerable<T> GetAll();
    }
}
