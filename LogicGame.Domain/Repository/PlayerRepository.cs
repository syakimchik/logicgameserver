﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogicGame.Domain.Database;

namespace LogicGame.Domain.Repository
{
    public class PlayerRepository : BaseRepository<Player>
    {
        private static int counter = 4;

        public override IEnumerable<Player> GetList()
        {
          return  _dbContext.Players.ToList();
        }

        public override Player GetById(long id)
        {
            return _dbContext.Players.Find(id);
        }

        public override void Create(Player entity)
        {
            _dbContext.Players.Add(entity);
            _dbContext.SaveChanges();
        }

        public override void Update(Player entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public override void Delete(Player entity)
        {
            if (entity != null)
            {
                _dbContext.Players.Remove(entity);
                _dbContext.SaveChanges();
            }
        }

        public override IEnumerable<Player> GetAll()
        {
            return _dbContext.Players;
        }
    }
}
