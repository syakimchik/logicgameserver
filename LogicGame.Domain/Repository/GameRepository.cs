﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogicGame.Domain.Database;

namespace LogicGame.Domain.Repository
{
    public class GameRepository: BaseRepository<Game>
    {
        public override IEnumerable<Game> GetList()
        {
           return _dbContext.Games.ToList();
        }

        public override Game GetById(long id)
        {
            return _dbContext.Games.Find(id);
        }

        public override void Create(Game entity)
        {
            _dbContext.Games.Add(entity);
            _dbContext.SaveChanges();
        }

        public override void Update(Game entity)
        {
            //_dbContext.Games.AddOrUpdate(entity);
            _dbContext.Entry(entity).State = EntityState.Modified; 
            _dbContext.SaveChanges();
        }

        public override void Delete(Game entity)
        {
            if (entity!= null)
            {
                _dbContext.Games.Remove(entity);
                _dbContext.SaveChanges();
            }
        }

        public override IEnumerable<Game> GetAll()
        {
            return _dbContext.Games;
        }

        public Game GetByName(string name)
        {
            return _dbContext.Games.FirstOrDefault(x => x.Name.Equals(name));
        }
    }
}
