﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogicGame.Domain.Database;

namespace LogicGame.Domain.Repository
{
    public class ScoreRepository: BaseRepository<Score>
    {
        private static int counter = 2;

        public override IEnumerable<Score> GetList()
        {
            throw new NotImplementedException();
        }

        public override Score GetById(long id)
        {
            throw new NotImplementedException();
        }

        public override void Create(Score entity)
        {
            if (entity.Id == 0)
            {
                entity.Id = counter++;
            }
            _dbContext.Scores.Add(entity);
            _dbContext.SaveChanges();
        }

        public override void Update(Score entity)
        {
            var obj = _dbContext.Scores.Find(entity.Id);
            obj.Count = entity.Count;
            _dbContext.SaveChanges();
        }

        public override void Delete(Score entity)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Score> GetAll()
        {
            return _dbContext.Scores;
        }
    }
}
