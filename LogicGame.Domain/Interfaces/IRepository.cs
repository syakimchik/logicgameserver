﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicGame.Domain.Interfaces
{
    /// <summary>
    /// Defines interface for common data access functionality for entity
    /// </summary>
    /// <typeparam name="T">Type of entry</typeparam>
    public interface IRepository<T>
    {
        IEnumerable<T> GetList();
        T GetById(long id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
    }
}
